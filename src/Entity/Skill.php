<?php

namespace App\Entity;

use App\Repository\SkillRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SkillRepository::class)
 */
class Skill
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $name;

    public function __toString() {
        return $this->name;
    }

    /**
     * @ORM\Column(type="boolean")
     */
    private ?bool $liked;

    /**
     * @ORM\ManyToOne(targetEntity=SkillCat::class)
     */
    private ?SkillCat $skill_cat;

    /**
     * @ORM\ManyToOne(targetEntity=Level::class)
     */
    private ?Level $level;

    /**
     * @ORM\ManyToMany(targetEntity=Profile::class, mappedBy="skills")
     */
    private Collection $profiles;

    public function __construct()
    {
        $this->profiles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLiked(): ?bool
    {
        return $this->liked;
    }

    public function setLiked(bool $liked): self
    {
        $this->liked = $liked;

        return $this;
    }

    public function getSkillCat(): ?SkillCat
    {
        return $this->skill_cat;
    }

    public function setSkillCat(?SkillCat $skill_cat): self
    {
        $this->skill_cat = $skill_cat;

        return $this;
    }

    public function getLevel(): ?Level
    {
        return $this->level;
    }

    public function setLevel(?Level $level): self
    {
        $this->level = $level;

        return $this;
    }

    /**
     * @return Collection|Profile[]
     */
    public function getProfiles(): Collection
    {
        return $this->profiles;
    }

    public function addProfile(Profile $profile): self
    {
        if (!$this->profiles->contains($profile)) {
            $this->profiles[] = $profile;
            $profile->addSkill($this);
        }

        return $this;
    }

    public function removeProfile(Profile $profile): self
    {
        if ($this->profiles->removeElement($profile)) {
            $profile->removeSkill($this);
        }

        return $this;
    }
}
