<?php

namespace App\Form;

use App\Entity\Profile;
use App\Entity\UserCat;
use App\Entity\Skill;
use App\Entity\History;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('first_name', TextType::class, [
                'label' => 'Your first name',
                'attr' => [
                    'placeholder' => 'Leila'
                ]
            ])
            ->add('last_name', TextType::class, [
                'label' => 'Your last name',
                'attr' => [
                    'placeholder' => 'Smith'
                ]
            ])
            ->add('address', TextType::class, [
                'label' => 'Your address',
                'attr' => [
                    'placeholder' => '21 Mean Street'
                ]
            ])
            ->add('zipcode', IntegerType::class, [
                'label' => 'Zip Code',
                'attr' => [
                    'placeholder' => '00000'
                ]
            ])
            ->add('city', TextType::class, [
                'label' => 'Your city',
                'attr' => [
                    'placeholder' => 'London'
                ]
            ])
            ->add('email', TextType::class, [
                'label' => 'Your email',
                'attr' => [
                    'placeholder' => 'my.real.email@yeahsure.com'
                ]
            ])
            ->add('phone', TextType::class, [
                'label' => 'Your phone number',
                'attr' => [
                    'placeholder' => '0000000000'
                ]
            ])
            ->add('available', CheckboxType::class, [
                'label' => 'Available'
            ])
            ->add('user_cat', EntityType::class, [
                'label' => 'You are currently a ... :',
                'class' => UserCat::class,
                'choice_label' => 'name',
                'expanded' => true
//                'multiple'     => true
            ])
            ->add('skills', EntityType::class, [
                'class' => Skill::class,
                'choice_label' => function($skill) {
                    return $skill->getName() . ' - ' . $skill->getSkillCat();
                },
                'expanded' => true,
                'multiple' => true,
                'group_by' => function($skill){
                    return $skill->getSkillCat();
                }
            ])
//            ->add('history', History::class, [
//                'label' => 'Your history'
//            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Profile::class,
        ]);
    }
}
