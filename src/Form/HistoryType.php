<?php

namespace App\Form;

use App\Entity\Company;
use App\Entity\History;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HistoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('position', TextType::class, [
                'label' => 'Position',
                'attr' => [
                    'placeholder' => 'e.g. Project Manager'
                ]
            ])
            ->add('place', TextType::class, [
                'label' => 'Where ?',
                'attr' => [
                    'placeholder' => 'e.g. London'
                ]
            ])
            ->add('start_date', DateType::class, [
                'widget' => 'single_text'
            ])
            ->add('end_date', DateType::class, [
                'widget' => 'single_text'
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Describe this experience',
                'attr' => [
                    'placeholder' => '...'
                ]
            ])
            ->add('context', TextareaType::class, [
                'label' => 'Context',
                'attr' => [
                    'placeholder' => '...'
                ]
            ])
            ->add('technical_env', TextType::class, [
                'label' => 'Technical environment',
                'attr' => [
                    'placeholder' => 'e.g. Node.js, React, Kubernetes, etc.'
                ]
            ])
            ->add('company', EntityType::class, [
                'class' => Company::class,
                'choice_label' => 'name',
                'expanded' => true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => History::class,
        ]);
    }
}
