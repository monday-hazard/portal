<?php

namespace App\Controller;

use App\Entity\Profile;
use App\Form\ProfileType;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProfileController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    
    /**
     * @Route("{id}/profile", name="profile")
     */
    public function index(User $user): Response
    {   
        $profiles = $this->entityManager->getRepository(Profile::class)->findAll();
        $profile = $this->entityManager->getRepository(Profile::class)->find($this->getUser());
        return $this->render('profile/index.html.twig', [
            'controller_name' => 'ProfileController',
            'profile' => $profile,
        ]);
    }

    /**
     * @Route("{id}/profile/new", name="new_profile")
     */
    public function new(Request $request, User $user)
    {
        $profile = new Profile();
        $form = $this->createForm(ProfileType::class, $profile);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $profile = $form->getData();

            $this->entityManager->persist($profile);
            $this->entityManager->flush();
            $user->setProfile($profile);
            $this->entityManager->flush();

            return $this->redirectToRoute('profile');
        }

        return $this->render(
            'profile/new.html.twig',
            ['form_profile' => $form->createView()]
        );
    }
}
