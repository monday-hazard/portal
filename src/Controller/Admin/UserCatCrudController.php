<?php

namespace App\Controller\Admin;

use App\Entity\UserCat;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class UserCatCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return UserCat::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name')
        ];
    }

}
