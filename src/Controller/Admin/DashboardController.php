<?php

namespace App\Controller\Admin;

use App\Entity\Company;
use App\Entity\History;
use App\Entity\Profile;
use App\Entity\Skill;
use App\Entity\SkillCat;
use App\Entity\User;
use App\Entity\UserCat;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\UserMenu;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        $collaborators = $this->getDoctrine()->getRepository(User::class)->findAll();
        $companies = $this->getDoctrine()->getRepository(Company::class)->findAll();
        $skillCategories = $this->getDoctrine()->getRepository(SkillCat::class)->findAll();
        $skills = $this->getDoctrine()->getRepository(Skill::class)->findAll();
        $userCategories = $this->getDoctrine()->getRepository(UserCat::class)->findAll();
        $profiles = $this->getDoctrine()->getRepository(Profile::class)->findAll();
        $histories = $this->getDoctrine()->getRepository(History::class)->findAll();

        return $this->render('bundles/EasyAdminBundle/dashboard.html.twig',
             [
                 'collaborators' => $collaborators,
                 'companies' => $companies,
                 'skillCategories' => $skillCategories,
                 'skills' => $skills,
                 'userCategories' => $userCategories,
                 'profiles' => $profiles,
                 'history' => $histories
             ]);
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Portal Admin');
    }

    public function configureUserMenu(UserInterface $user): UserMenu
    {
        return parent::configureUserMenu($user)
            // use the given $user object to get the user name
            ->setName($user->getUsername())
            // use this method if you don't want to display the user image
            ->displayUserAvatar(false)

            // you can use any type of menu item, except submenus
            ->addMenuItems([
                               MenuItem::linkToRoute('My Profile', 'fa fa-id-card', '/profile'),
                           ]);
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');

        if ($this->isGranted('ROLE_ADMIN')) {
            yield MenuItem::linkToCrud('Users', 'fas fa-list', User::class);
            yield MenuItem::linkToCrud('User Categories', 'fas fa-list', UserCat::class);
            yield MenuItem::linkToCrud('Companies', 'fas fa-list', Company::class);
            yield MenuItem::linkToCrud('Skills', 'fas fa-list', Skill::class);
            yield MenuItem::linkToCrud('Skill Categories', 'fas fa-list', SkillCat::class);
        }
        yield MenuItem::linkToCrud('Profiles', 'fas fa-list', Profile::class);
        yield MenuItem::linkToCrud('Histories', 'fas fa-list', History::class);
        yield MenuItem::linkToLogout('app_logout', 'fa fa-sign-out');
    }
}
