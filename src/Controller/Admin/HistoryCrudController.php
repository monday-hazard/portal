<?php

namespace App\Controller\Admin;

use App\Entity\History;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class HistoryCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return History::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('position'),
            TextField::new('place'),
            AssociationField::new('company'),
            DateTimeField::new('start_date'),
            DateTimeField::new('end_date'),
            TextEditorField::new('description'),
            TextField::new('technical_env')
        ];
    }
}
