<?php

namespace App\Controller\Admin;

use App\Entity\Profile;
use Doctrine\Common\Collections\Collection;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ProfileCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Profile::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('first_name'),
            TextField::new('last_name'),
            AssociationField::new('user_cat'),
            TextField::new('address'),
            IntegerField::new('zipcode'),
            TextField::new('city'),
            TextField::new('email'),
            TextField::new('phone'),
            BooleanField::new('available'),
            AssociationField::new('skills'),
            AssociationField::new('history')
        ];
    }

}
