<?php

namespace App\Controller;

use App\Entity\History;
use App\Entity\User;
use App\Entity\Profile;
use App\Form\HistoryType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HistoryController extends AbstractController
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("{id}/history", name="history")
     */
    public function index(User $user): Response
    {
        $history = $this->entityManager->getRepository(History::class)->find($this->getUser());
        return $this->render('history/index.html.twig', [
            'controller_name' => 'HistoryController',
            'history' => $history
        ]);
    }

    /**
     * @Route("{id}/history/new", name="new_history")
     */
    public function new(Request $request, Profile $profile )
    {
        $history = new History();
        $form = $this->createForm(HistoryType::class, $history);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $history = $form->getData();

            $this->entityManager->persist($history);
            $this->entityManager->flush();
            $profile->addHistory($history);
            $this->entityManager->flush();

            return $this->redirectToRoute('history');
        }

        return $this->render(
            'history/new.html.twig',
            ['form_history' => $form->createView()]
        );
    }
}
