# Portal

[![forthebadge](http://forthebadge.com/images/badges/built-with-love.svg)](http://forthebadge.com)  [![forthebadge](http://forthebadge.com/images/badges/powered-by-electricity.svg)](http://forthebadge.com)

Le projet Portal a pour objectif d'être un outil de gestion des compétences permettant de centraliser les informations des collaborateurs.

## Pour commencer

### Pré-requis

Etre armé de patience et d'indulgence.

### Installation

1. Cloner le projet:
- git clone https://gitlab.com/monday-hazard/portal.git

2. Dans le projet installer Webpack encore et Sass:
- npm install 
- npm run build

3. Dans phpMyAdmin, créer la base 'portal' et copier le contenu du fichier create-for-db-portal.txt joint pour peupler la base avec quelques exemples de données.

4. Créer un fichier env.local et y établir la connection avec la base de données 'portal'.

Possible: Faire les migrations pour créer les entités. Et remplir la base à la main.


## Démarrage

1. Lancer Symfony:
- symfony serve

2. Se rendre sur son localhost et, au choix: 
- se créer un utilisateur (role user, manager ou admin)
- si on a créé les tables grâce au fichier create-for-db-portal.txt, utiliser l'identifiant suivant:
leila@gmail.com 
  mot de passe: leila

## Bonne visite !





